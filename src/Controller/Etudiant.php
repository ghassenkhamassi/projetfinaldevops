<?php

namespace App\Controller;

class Etudiant
{
private $id;
private $nom;
private $prenom;
private $matieres = [];

    /**
     * @param $id
     * @param $nom
     * @param $prenom
     * @param array $matieres
     */
    public function __construct($id, $nom, $prenom, array $matieres)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->matieres = $matieres;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return array
     */
    public function getMatieres(): array
    {
        return $this->matieres;
    }

    /**
     * @param array $matieres
     */
    public function setMatieres(array $matieres): void
    {
        $this->matieres = $matieres;
    }

}