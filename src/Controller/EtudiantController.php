<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class EtudiantController extends AbstractController
{
    #[Route('/etudiant', name: 'app_etudiant')]
    public function index(): Response
    {
        return new Response("Bonjour");
    }

    #[Route('/etudiant/{id}', name:'etudiantId',
        requirements:["id"=>"\d{3}"])]
public function viewEtudiant($id){
        return new Response('L etudiant numéro'.$id);
    }

    #[Route('etudiant/{name}', name:'nameEtud',
        requirements: ["name"=>"[A-Z0-9]{2,6}"])]
public function voirEtudiant($name){
        return $this->render('etudiant/etudiant.html.twig',
            ["nom"=>$name]);
    }

    #[Route('/etudiant/{login}/{password}', name:'login')]
    public function login($login, $password){
        return $this->render('etudiant/login.html.twig',
        ["log"=>$login, "pass"=>$password]);
    }

    #[Route('/liste', name: 'liste')]
    public function listeEtudiant(){
        $etudiants= ["ali", "mohamed"];
        $modules = [
            ["id"=>1,
                "nom"=>"Symfony",
                "enseignant"=>"Med",
                "nbrHeures"=>42,
                "date"=>"17-02-23"],
            ["id"=>2,
                "nom"=>"SB",
                "enseignant"=>"ALI",
                "nbrHeures"=>42,
                "date"=>"18-08-23"],
            ["id"=>3,
                "nom"=>"Anglais",
                "enseignant"=>"Hela",
                "nbrHeures"=>21,
                "date"=>"12-02-23"]
        ];
        return $this->render("etudiant/list.html.twig",
        array("etudiants"=>$etudiants,
            "listeModules"=>$modules));
    }

    #[Route('/affecter', name: 'Affectation')]
    public function affecter(){
        return $this->render('etudiant/affecter.html.twig');
    }
    #[Route('/fils', name:'fils')]
public function filsTest(){
        return $this->render("etudiant/index.html.twig");
    }

    #[Route('/listeP', name:"listeProduit")]
public function listeProduits(){
        //Liste des clients
        $clients = [
            ["id"=>1, "nom"=>"ali"],
            ["id"=>2, "nom"=>"med"],
            ["id"=>3, "nom"=>"salah"]
        ];
        //Liste des produits
        $produits = [
            ["id"=>1, "design"=>"p1", "prix"=>30, "idClient"=>1],
            ["id"=>2, "design"=>"p2", "prix"=>10, "idClient"=>3],
            ["id"=>3, "design"=>"p3", "prix"=>50, "idClient"=>3],
            ["id"=>4, "design"=>"p4", "prix"=>50, "idClient"=>1],
            ["id"=>5, "design"=>"p5", "prix"=>15, "idClient"=>2],
            ["id"=>6, "design"=>"p6", "prix"=>20, "idClient"=>1]
        ];
        return $this->render('produit/produits.html.twig',
        ["listeClient"=>$clients, "listeProduit"=>$produits]);
    }
}
