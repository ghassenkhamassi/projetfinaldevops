<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MatiereController extends AbstractController
{
    #[Route('/matiere', name: 'app_matiere')]
    public function index(): Response
    {
        return $this->render('matiere/index.html.twig', [
            'controller_name' => 'MatiereController',
        ]);
    }

    #[Route('/listeM', name:"listeM")]
public function listeMatieres(){
        //Des matières
        $m1 = new Matiere(1, "Symfony");
        $m2 = new Matiere(2, "SB");
        $m3 = new Matiere(3, "JAVA");

        //Des étudiants
        $e1 = new Etudiant(1, "X", "Y", [$m1, $m3]);
        $e2 = new Etudiant(2, "X1", "Y1", [$m2]);
        $e3 = new Etudiant(3, "X2", "Y2", [$m1, $m3, $m2]);
        //Liste des étudiants
        $etudiants = [$e1, $e2, $e3];
        return $this->render('matiere/listeM.html.twig',
        ["listeE"=>$etudiants]);
    }
}
